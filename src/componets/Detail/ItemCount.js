import React, { useState } from "react";

export const ItemCount = ({ itemStock, onAdd }) => {
  const Swal = require('sweetalert2')
  // [count, setCount]
  const [contador, setContador] = useState(1);

  const handlerClick = () => {
    if (contador < itemStock) {
      setContador(contador + 1);
    } else {
      Swal.fire({
        icon: 'error',
        title: 'Oops...',
        text: 'Stock Maximo',
        footer: 'No se puede agregar mas unidades'
      })
    }
  }

  // Mejor en ingles, tanto metodos como variables
  // todo lo que quita:remove... EJ: removeItem, removeArticle, removeProduct. etc

  const quitar = () => {
    if (contador > 0) {
      setContador(contador - 1);
    } else {
      (Swal.fire({
        icon: 'error',
        title: 'Oops...',
        text: 'No se puede seguir quitando',

      }))
    }
  }

  const agregarCarrito = () => {
    if (contador !== 0) {
      onAdd(contador)
    }
    else {
      (Swal.fire({
        icon: 'error',
        title: 'Oops...',
        text: 'Ingrese una cantidad correcta por favor',

      }))

    }
  };

  return (

    <div className="itemCount">

      <ul className="btnCard">
        <li> <button onClick={handlerClick}>Agregar</button></li>
        <li><div className="contador">{contador}</div></li>
        <li><button onClick={quitar}>Quitar</button></li>
      </ul>
      <button className="agregarCarrito" onClick={agregarCarrito}>Agregar a Carrito</button>
    </div>
  )
}

  // En handlerclick(), quitar(),agregarCarrito()   se hace un Swal, Se podria hacer un metodo que dispare el Swal,
  // pasando como paremetro el text y el footer si lo tien
  // y lo reusas en los 3 metodos