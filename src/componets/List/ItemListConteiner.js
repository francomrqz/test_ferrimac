import React, { useState, useEffect } from "react";
import ItemList from "./ItemList";
import { useParams } from "react-router-dom";
import { db } from "../../firebase/firebase"
import { getDocs, collection, query, where } from "firebase/firestore"

const ItemListConteiner = () => {

  const { categoryId } = useParams();
  //[products, setProductos] or [products, addProducts]
  const [productos, setProductos] = useState([])

  useEffect(() => {

    const productCollection = collection(db, 'productos')
    const q = query(productCollection, where('category', '==', `${categoryId}`))

    getDocs(categoryId ? q : productCollection).then(result => {
      const lista = result.docs.map(doc => {
        return {
          id: doc.id,
          ...doc.data(),
        }
      })
      setTimeout(() => {
        setProductos(lista)
      }, 2000)
    }).catch((err) => {
      console.log(err);
    })

  }, [categoryId])

  return (
    <div>
      {<ItemList className="itemList" items={productos} />}
    </div>
  )

}

export default ItemListConteiner